;;; criticalmarks.el ---                             -*- lexical-binding: t; -*-

;; Copyright (C) 2019  Juan Manuel Macías

;; Author: Juan Manuel Macías <maciaschain@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary: A set of functions for Gnu Emacs that run in the
;;; export process from Org Mode to LaTeX. Its aim is to replace a
;;; series of simple textual marks in the basic macros of the
;;; reledmac.sty package for critical editions. The critical and
;;; familiar notes of reledmac can be added to the *org file as simple
;;; native footnotes.

;;

;;; Code:

(require 'ox)

(defun footnote-apparatus (text backend info)
  "apparatus and familiar notes"
  (when (org-export-derived-backend-p backend 'latex)
   (replace-regexp-in-string "\\\\footnote{\\(.*\\)\\([A-Z]\\):::\s*" (concat "{" "\\1" "\\\\" "\\2" "footnote{")
   (replace-regexp-in-string "\\\\footnote{F\\([A-Z]\\):::\s*" "\\\\footnote\\1{"
   (replace-regexp-in-string " @!" "}" text)))))

(defun critical-marks (text backend info)
    "edtext y lemmas"
    (when (org-export-derived-backend-p backend 'latex)
     (replace-regexp-in-string "((" "\\\\edtext{"
     (replace-regexp-in-string "))" "}"
     (replace-regexp-in-string "\s*;;\\(.+\\),\\(.+\\);;\s*" "\\\\xxref{\\1}{\\2}"
     (replace-regexp-in-string "<@" "\\\\edlabel{"
     (replace-regexp-in-string "@>" "}"
     (replace-regexp-in-string "\s*->\s*\\(.\\):\s*" (concat "}\\\\" "\\1" "footnote{")
     (replace-regexp-in-string "\s*<!\s*" "}\\\\lemma{"
     (replace-regexp-in-string "\s*!>\s" "}"
     (replace-regexp-in-string "'''" "\\\\beginnumbering\\\\pstart\n"
     (replace-regexp-in-string "'''!" "\n\\\\pend\\\\endnumbering"
     (replace-regexp-in-string ",,," "\n\\\\pend\\\\pstart\n" text)))))))))))))

(defun critical-corrections (text backend info)
    "dirty corr."
    (when (org-export-derived-backend-p backend 'latex)
      (replace-regexp-in-string "}}\\(\\\\.footnote\\)" "}\\1"
      (replace-regexp-in-string "}}\\(\\\\lemma\\)" "}\\1"
      (replace-regexp-in-string "{}\\\\lemma{" "{\\\\lemma{"  text)))))

(add-to-list 'org-export-filter-footnote-reference-functions 'footnote-apparatus)
(add-to-list 'org-export-filter-plain-text-functions 'critical-marks)
(add-to-list 'org-export-filter-final-output-functions 'critical-corrections)

; Highlighting

(defvar edtext (make-face 'edtext))
(defvar lemma (make-face 'lemma))
(defvar edlabel (make-face 'edlabel))
(defvar xxref (make-face 'xxref))
(set-face-foreground 'edtext "magenta")
(set-face-foreground 'lemma "deep sky blue")
(set-face-foreground 'edlabel "chocolate")
(set-face-foreground 'xxref "red")

(font-lock-add-keywords
 'org-mode
 '(("((" 0 edtext)
   ("))" 0 edtext)
   ("<!" 0 lemma)
   ("!>" 0 lemma)
   ("<@" 0 edlabel)
   ("@>" 0 edlabel)
   (";;\\(.+\\),\\(.+\\);;" 0 xxref)))

(provide 'criticalmarks)
;;; criticalmarks.el ends here
