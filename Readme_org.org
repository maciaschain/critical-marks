#+TITLE: critical-marks.el

* Contents
  :PROPERTIES:
  :TOC:      this
  :END:
-  [[#description][Description]]
-  [[#installation][Installation]]
  -  [[#use-locally][Use locally]]
-  [[#textual-marks][Textual marks]]
-  [[#notes][Notes]]
-  [[#screenshots][Screenshots]]

* Description
  :PROPERTIES:
  :CUSTOM_ID: Description
  :END:

A set of functions for Gnu Emacs that run in the export process from Org Mode to LaTeX. Its aim is to replace a series
of simple textual marks with the basic macros of the [[https://github.com/maieul/ledmac][reledmac.sty]] package for critical editions. The critical and
familiar notes of ~reledmac~ can be added to the ~*org~ file as simple native footnotes.

* Installation
  :PROPERTIES:
  :CUSTOM_ID: Installation
  :END:

Put the ~critical-marks.el~ file in your load-path and add to your init file the line:

#+begin_src emacs-lisp
(load "critical-marks.el")
#+end_src

** Use locally
   :PROPERTIES:
   :CUSTOM_ID: Use-locally
   :END:

You can also test these filters locally in an individual file. To do this, enclose the code in a source block at the
beginning of your document:

#+begin_src org
,#+BIND: org-export-filter-footnote-reference-functions (footnote-apparatus)
,#+BIND: org-export-filter-plain-text-functions (critical-marks)
,#+BIND: org-export-filter-final-output-functions (critical-corrections)
,#+BEGIN_SRC emacs-lisp :exports results :results none
< ... code ... >
,#+END_SRC
#+end_src

and add these lines to the end of the document:

#+begin_src org
# Local Variables:
# org-export-allow-bind-keywords: t
# End:
#+end_src

* Textual marks
  :PROPERTIES:
  :CUSTOM_ID: Textual-marks
  :END:
|-------------------------+-------------------------------|
| marks in Org            | ~reledmac~ commands           |
|-------------------------+-------------------------------|
| ~((lorem ipsum dolor))~ | ~\edtext{lorem ipsum dolor}~  |
| ~<!lorem ipsum dolor!>~ | ~\lemma{lorem ipsum dolor}~   |
| ~<@lorem ipsum dolor@>~ | ~\edlabel{lorem ipsum dolor}~ |
| ~;;label1,label2;;~     | ~\xxref{label1}{label2}~      |
| '''                     | ~\beginnumbering\pstart~      |
| ,,,                     | ~\pend\pstart~                |
| '''!                    | ~\pend\endnumbering\~         |
|-------------------------+-------------------------------|

* Notes
  :PROPERTIES:
  :CUSTOM_ID: Notes
  :END:

In order for a note to be exported as a critical note, it must start with ~A:~, ~B:~ or ~C:~. This will be exported as
critical apparatus note of series A, B or C. *All critical notes must end with a ~@!~*. For example, a string like this:

 #+begin_src org
((lorem ipsum dolor))[fn:1]

...

[fn:1]A: This is a note @!
 #+end_src

will be exported to LaTeX as:

#+begin_src latex :exports code
\edtext{lorem ipsum dolor}{\Afootnote{This is a note}}
#+end_src

=lemma= and =xxref= commands must be written before the text of the note. For example, a string like this:

#+begin_src org
((lorem ipsum dolor))[fn:1]

...

[fn:1];;label1,label2;; <!A lemma!> A: This is a note @!
#+end_src

will be exported to LaTeX as:

#+begin_src latex :exports code
\edtext{lorem ipsum dolor}{\xxref{label1}{label2}\lemma{A lemma}\Afootnote{This is a note}}
#+end_src

Several notes of different series related to a passage enclosed in an =edtext= macro should be written in a row, separated by a "->". A
string like this:

#+begin_src org
((lorem ipsum dolor))[fn:1]

...

[fn:1]A: a critical footnote -> B: another note @!
#+end_src

will be exported to LaTeX as:

#+begin_src latex :exports code
\edtext{lorem ipsum dolor}{\Afootnote{a critical footnote}\Bfootnote{another note}}
#+end_src

Or a string like this:

#+begin_src org
((lorem ipsum dolor))[fn:1]

...

[fn:1]A: a critical footnote <!a lemma for the next note!> -> B: another note @!
#+end_src

will be exported to LaTeX as:

#+begin_src latex :exports code
\edtext{lorem ipsum dolor}{\Afootnote{a critical footnote}\lemma{a lemma for the next note}\Bfootnote{another note}}
#+end_src

In order for a note to be exported as a familiar note, it must start with ~FA:~, ~FB:~ o ~FC:~.

* Screenshots
  :PROPERTIES:
  :CUSTOM_ID: Screenshots
  :END:

[[./screenshot2.png]]

[[./screenshot1.png]]
